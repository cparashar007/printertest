package com.example.printertest.sharedpreferences;

public class SharedPreferencesName
{

    /**
     * Class Contains all Local variables on app..
     */

    // LOGIN CREDENTIALS
    public static String USER_MOBILE_NUMBER = "user_mobile_number";
    public static String USER_PASSWORD = "user_password";
    public static String NUMBER_OF_INVOICES = "invoices";
    public static String NUMBER_OF_PICKUPS = "pickups";

	// CONFIG VALUES
	public static String SHOULD_MONITOR = "should_monitor";
	public static String ARRIVAL_LOCATION = "arrival_location";
	public static String FLOW_TYPE = "flow_type";

    // ADDRESS VARIABLES
    public static String DRIVER_ID = "d_id";
    public static String DRIVER_TYPE = "d_type";
    public static String EXPIRY_DATE = "exp_date";
    public static String LICENCE = "licence";
    public static String DEVICE_TOKEN = "deviceToken";
    public static String ACCESSTOKEN = "accessToken";
    public static String LOCATION = "location";
    public static String MOBILE = "mobile";
    public static String NAME = "name";
    public static String STATUS = "status";
    public static String VEHICLE_ID = "v_id";
    public static String VEHICLE_TYPE = "v_type";
    public static String Profile_PIC = "original";
    public static String SHOW_LOCKTAG = "no";
    public static String TEMP_LOCKTAG_FILE = "temp_file";
    public static int c_frag = 888;
    public static String SELECTED_LANGUAGE = "lang";
    public static String LOCKTAG_CHANGED = "no";
    public static String SIGNATURE_DISABLED = "disabled";
    public static String DRIVER_RATING = "1";
    public static String SIGNATURE_FILE_PATH = "xyz";
    public static String LOCKTAG_PIC_PATH_1 = "path_1";
    public static String LOCKTAG_PIC_PATH_2 = "path_2";
    public static String LOCKTAG_PIC_PATH_3 = "path_3";
    public static String LOCKTAG_PIC_PATH_4 = "path_4";
    public static String PICKUP_FINISH_TIME = "pickup_time";
    public static String DELIVERY_FINISH_TIME = "delivery_time";
    public static String BLUETOOTH_ADDRESS = "bluetooth_address";
    public static String CURRENT_SHIPMENT_ID = "current_shipment_id";
    public static String LOCAL_SHIPMENT_ID = "local_shipment_id";
	public static String TOTAL_PACKAGES = "total_packages_1";
	public static String EPOD_RESPONSE = "epod_response";
    public static String ATTENDANCE_MARKED = "attendance_marked";
    public static String CALCULATION_FACTOR = "calculation_factor";
    public static String DISTANCE_FACTOR = "distance_factor";
    public static String DISTANCE_THRESHOLD = "distance_threshold";
    public static String TIME_FACTOR = "time_factor";
    public static String CURRENT_LOCATION = "current_location";
	public static String LAST_LAT = "last_lat";
	public static String LAST_LNG = "last_lng";
	public static String LAST_LOCATION = "last_location";
	public static String DELIVERY_PACKAGES = "delivery_packages";
}
