package com.example.printertest;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.printertest.sharedpreferences.Prefs;
import com.example.printertest.sharedpreferences.SharedPreferencesName;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class SearchBTActivity extends Activity implements OnClickListener
{
    private static Handler mHandler = null;
    private static String TAG = "SearchBTActivity";
    private LinearLayout linearlayoutdevices;
    private ProgressBar progressBarSearchStatus;
    private ProgressDialog dialog;
    private BroadcastReceiver broadcastReceiver = null;
    private IntentFilter intentFilter = null;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchbt);
        askForBluetoothPermissions();
        findViewById(R.id.buttonSearch).setOnClickListener(this);
        progressBarSearchStatus = (ProgressBar) findViewById(R.id.progressBarSearchStatus);
        progressBarSearchStatus.setVisibility(View.INVISIBLE);

        linearlayoutdevices = (LinearLayout) findViewById(R.id.linearlayoutdevices);
        dialog = new ProgressDialog(this);

        initBroadcast();

        try
        {
            if (Prefs.with(this).getString(SharedPreferencesName.BLUETOOTH_ADDRESS, null) != null)
            {
                mHandler = new MHandler(this);
                WorkService.addHandler(mHandler);

                BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

                if (!adapter.isEnabled())
                {
                    if (adapter.enable())
                    {
                        while (!adapter.isEnabled())
                        {

                        }
                        Log.v(TAG, "Enable BluetoothAdapter");
                    }
                    else
                    {
                        finish();
                    }
                }

                try
                {
                    String deviceAddress = Prefs.with(SearchBTActivity.this).getString(SharedPreferencesName.BLUETOOTH_ADDRESS, "");

//                    WorkService.workThread.disconnectBt();

                    dialog.setMessage(Global.toast_connecting + " " + deviceAddress);
                    dialog.setIndeterminate(true);
                    dialog.setCancelable(true);
                    dialog.show();

                    Prefs.with(SearchBTActivity.this).save(SharedPreferencesName.BLUETOOTH_ADDRESS, deviceAddress);

                    WorkService.workThread.connectBt(deviceAddress);
                }
                catch (Exception e)
                {
                    e.printStackTrace();

                    if (null == WorkService.workThread)
                    {
                        Intent intent = new Intent(SearchBTActivity.this, WorkService.class);
                        startService(intent);
                    }

                    Toast.makeText(SearchBTActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                mHandler = new MHandler(this);
                WorkService.addHandler(mHandler);

                searchBtnAction();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            mHandler = new MHandler(this);
            WorkService.addHandler(mHandler);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        WorkService.delHandler(mHandler);
        mHandler = null;
        uninitBroadcast();

//        PrintFragment.isPrinting = 0;
    }

    public void onClick(View arg0)
    {
        // TODO Auto-generated method stub
        switch (arg0.getId())
        {
            case R.id.buttonSearch:
            {
//                progressBarSearchStatus.setVisibility(View.VISIBLE);

                searchBtnAction();

                break;
            }
        }
    }

    private void searchBtnAction()
    {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

        if (null == adapter)
        {
            finish();
            return;
        }

        if (!adapter.isEnabled())
        {
            if (adapter.enable())
            {
                while (!adapter.isEnabled())
                {

                }
                Log.v(TAG, "Enable BluetoothAdapter");
            }
            else
            {
                finish();
                return;
            }
        }

        if (null != WorkService.workThread)
        {
            WorkService.workThread.disconnectBt();
            TimeUtils.WaitMs(10);
        }

        adapter.cancelDiscovery();
        linearlayoutdevices.removeAllViews();
        TimeUtils.WaitMs(10);
        adapter.startDiscovery();
    }

    public void closeSearchWindow(View v)
    {
        finish();
//        PrintFragment.isPrinting = 0;
    }

    private void initBroadcast()
    {
        broadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                // TODO Auto-generated method stub
                String action = intent.getAction();
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (BluetoothDevice.ACTION_FOUND.equals(action))
                {
                    progressBarSearchStatus.setVisibility(View.VISIBLE);

                    if (device == null)
                    {
                        return;
                    }
                    final String address = device.getAddress();
                    String name = device.getName();
                    if (name == null)
                    {
                        name = "BT";
                    }
                    else if (name.equals(address))
                    {
                        name = "BT";
                    }
                    Button button = new Button(context);
                    button.setText(name + ": " + address);
                    button.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
                    button.setOnClickListener(new OnClickListener()
                    {
                        public void onClick(View arg0)
                        {
                            // TODO Auto-generated method stub
                            try
                            {
                                WorkService.workThread.disconnectBt();
                                // 只有没有连接且没有在用，这个才能改变状态
                                dialog.setMessage(Global.toast_connecting + " " + address);
                                dialog.setIndeterminate(true);
                                dialog.setCancelable(true);
                                dialog.show();

//                                Prefs.with(SearchBTActivity.this).save(SharedPreferencesName.BLUETOOTH_ADDRESS, address);

                                WorkService.workThread.connectBt(address);

//								finish();
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();

                                if (null == WorkService.workThread)
                                {
                                    Intent intent = new Intent(SearchBTActivity.this, WorkService.class);
                                    startService(intent);
                                }

                                Toast.makeText(SearchBTActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
//					button.getBackground().setAlpha(100);

//                    if(name.equalsIgnoreCase("Printer"))
//                    {
                    linearlayoutdevices.addView(button);
//                    }

                }
                else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action))
                {
                    progressBarSearchStatus.setIndeterminate(true);
                    progressBarSearchStatus.setVisibility(View.VISIBLE);
                }
                else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
                {
                    progressBarSearchStatus.setIndeterminate(false);
                    progressBarSearchStatus.setVisibility(View.INVISIBLE);

                }
            }
        };
        intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    // 8951788611 deepa
    private void uninitBroadcast()
    {
        if (broadcastReceiver != null)
        {
            unregisterReceiver(broadcastReceiver);
        }
    }

    class MHandler extends Handler
    {
        WeakReference<SearchBTActivity> mActivity;

        MHandler(SearchBTActivity activity)
        {
            mActivity = new WeakReference<SearchBTActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg)
        {
            SearchBTActivity theActivity = mActivity.get();
            switch (msg.what)
            {
                /**
                 * DrawerService 的 onStartCommand会发送这个消息
                 */

                case Global.MSG_WORKTHREAD_SEND_CONNECTBTRESULT:
                {
                    int result = msg.arg1;
                    Toast.makeText(theActivity, (result == 1) ? Global.toast_success : Global.toast_fail, Toast.LENGTH_SHORT).show();
//                    Toast.makeText(theActivity, (result == 1) ? Global.toast_success : Global.toast_wait, Toast.LENGTH_SHORT).show();
                    Log.v(TAG, "Connect Result: " + result);
                    theActivity.dialog.cancel();

                    int copies = 1;

                    try
                    {
                        copies = getIntent().getIntExtra("number", 1);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    if (1 == result)
                    {
                        for(int i=0; i<copies; i++)
                        {
                            PrintTest();
                        }
                    }
                    else
                    {
                        if (null == WorkService.workThread)
                        {
                            Intent intent = new Intent(SearchBTActivity.this, WorkService.class);
                            startService(intent);
                        }

//                        reinitializeBT();

                        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                        if (mBluetoothAdapter.isEnabled())
                        {
                            mBluetoothAdapter.disable();
                        }

                        Prefs.with(SearchBTActivity.this).remove(SharedPreferencesName.BLUETOOTH_ADDRESS);
                        finish();
                    }
                    break;
                }
            }
        }

        private void reinitializeBT()
        {
            try
            {
                if (Prefs.with(SearchBTActivity.this).getString(SharedPreferencesName.BLUETOOTH_ADDRESS, null) != null)
                {
                    try
                    {
                        String deviceAddress = Prefs.with(SearchBTActivity.this).getString(SharedPreferencesName.BLUETOOTH_ADDRESS, "");

                        WorkService.workThread.disconnectBt();
                        // 只有没有连接且没有在用，这个才能改变状态
                        dialog.setMessage(Global.toast_connecting + " " + deviceAddress);
                        dialog.setIndeterminate(true);
                        dialog.setCancelable(true);
                        dialog.show();

                        Prefs.with(SearchBTActivity.this).save(SharedPreferencesName.BLUETOOTH_ADDRESS, deviceAddress);

                        WorkService.workThread.connectBt(deviceAddress);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();

                        if (null == WorkService.workThread)
                        {
                            Intent intent = new Intent(SearchBTActivity.this, WorkService.class);
                            startService(intent);
                        }

                        Toast.makeText(SearchBTActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();

                Toast.makeText(mActivity.get(), "Failed to connect to Printer.", Toast.LENGTH_SHORT).show();
            }
        }

        void PrintTest()
        {
            File printFile = new File(Environment.getExternalStorageDirectory() + "/InvoiceText.txt");

            byte[] b = new byte[(int) printFile.length()];
            try
            {
                FileInputStream fileInputStream = new FileInputStream(printFile);
                fileInputStream.read(b);
                for (int i = 0; i < b.length; i++)
                {
                    System.out.print((char) b[i]);
                }
            }
            catch (FileNotFoundException e)
            {
                System.out.println("File Not Found.");
                e.printStackTrace();
            }
            catch (IOException e1)
            {
                System.out.println("Error Reading The File.");
                e1.printStackTrace();
            }

            byte[] buf = b;

            if (WorkService.workThread.isConnected())
            {
                Bundle data = new Bundle();
                data.putByteArray(Global.BYTESPARA1, buf);
                data.putInt(Global.INTPARA1, 0);
                data.putInt(Global.INTPARA2, buf.length);
                WorkService.workThread.handleCmd(Global.CMD_WRITE, data);

                finish();
            }
            else
            {
                Toast.makeText(mActivity.get(), Global.toast_notconnect, Toast.LENGTH_SHORT).show();
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void askForBluetoothPermissions() {
        String[] permissions = new String[] {
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN
        };
        requestPermissions(permissions, 101);
    }
}
